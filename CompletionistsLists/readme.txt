
How To Setup:
Install like other addons - by unziping into the Elder Scrolls Online\live\AddOns folder

How To Use:
1. Type /completionistslists
2. Choose an icon on the left side 
CP icon: List all recipies in the game
Helmet icon: List all known motifs, use the button at the bottom to choose if to list each page in own line, or one line per motif with 0 = unknown page, 1 = known page
Chair icon: List all known furniture recipies
Pot icon: List all known provisioning recipies
Star icon: List of all antiquities in game, nr of leads found and codex entries discovered
3. Click on the list, use ctrl + A to select all, and ctrl + c to copy
4. Go to either a notepad or calculator sheet (your choice!) and use ctrl + v to paste (or select in edit > paste or import data depending on your choice!). Column delimiter is semicolon ( ; ) See picture in how-to as example for open office calculator
5. Use arrow keys on top right corner of the addon window to navigate to the next page (see current page / total pages in bottom right corner of addon window) and repeat untill you have all the pages in your chosen sheet


Note: If you use the sample calculator sheet, remember to clear the lists before adding your own character info. You can select the upper corder left of A and above 1 to select all, and click delete.